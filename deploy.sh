#!/bin/bash
# (re)creates release and copies src, shaders and install.sh
rm -rf release
mkdir -p release/{src,lib,shaders}
cp -rL src/* release/src
cp shaders/* release/shaders
cp -p install.sh release/

# (re)creates the tar with an auxiliar directory
cp -r release release-$(date +%Y-%m-%d)
rm -f release.tar.gz
tar -pzcvf release.tar.gz release-$(date +%Y-%m-%d)
rm -rf release-$(date +%Y-%m-%d)

