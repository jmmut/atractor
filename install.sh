#!/bin/bash
# Refer to README.md for complete explanation. This script works just in the better case.
#
#### Dependencies
#
# This program requires SDL2 and randomize/common-libs
# SDL2 is available in apt-get:
#
sudo apt-get install libsdl2-dev
#
# randomize/common-libs can be downloaded with wget or git:
#
# $ wget https://bitbucket.org/mutcoll/randomize/get/master.zip; unzip master.zip
# or 
git clone https://bitbucket.org/mutcoll/randomize.git
# or 
# $ git clone git@bitbucket.org:mutcoll/randomize.git
#
# and then:
cd randomize/common-libs
make
sudo make install
cd ../../
#
#
#### Installing
#
# you can build the program with make, 
# the file is in the src directory

cd src
make
cd ..

# finally, to launch:
#./visor
