
//uniform vec2 camera;
//uniform float t;
uniform float camx;
uniform float camy;
uniform float camz;
uniform float threshold;
varying vec4 micolor;

void main()
{
	vec4 pos = gl_Vertex;
	//pos.z = pos.z* 0.2;
	//gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = gl_ModelViewProjectionMatrix * pos;
	gl_FrontColor = gl_Color;
	vec4 dist = gl_Vertex - vec4(camx, camy, camz, 1.0);
	float scalar_dist = length(dist);
	float range = threshold;
	float depth = range - scalar_dist;
	float col = depth/range + 0.2;
	//gl_FrontColor.x = scalar_dist/2.0; 
	//gl_FrontColor.y = camera.z/500.0;
	//gl_FrontColor.x = 0.5;
	gl_FrontColor.y = col;
	gl_FrontColor.x = col;
	micolor = gl_FrontColor;
	//gl_FrontColor.y = t;
	//gl_FrontColor.z = 0.5;
}

