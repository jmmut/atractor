
//uniform vec2 camera;
//uniform float t;
uniform float camx;
uniform float camy;
uniform float camz;
uniform float centerx;
uniform float centery;
uniform float centerz;
//uniform float threshold;
varying vec4 micolor;

void main()
{
	vec4 pos = gl_Vertex;
	//pos.z = pos.z* 0.2;
	//gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = gl_ModelViewProjectionMatrix * pos;

	gl_FrontColor = gl_Color;
	vec4 cam = vec4(camx, camy, camz, 1.0);
	vec4 center = vec4(centerx, centery, centerz, 1.0);
	vec4 center_point = pos - center;
	vec4 center_cam = cam - center;
	//float cos_a = dot(cam, pos)/(length(cam)*length(pos));
	float cos_a = dot(center_cam, center_point)/(length(center_cam)*length(center_point));

	//vec4 cam_point = gl_Vertex - cam;
	//float scalar_dist = length(cam_point);
	//float range = threshold;
	//float depth = range - scalar_dist;
	//float col = depth/range + 0.2;
	//gl_FrontColor.x = scalar_dist/2.0; 
	//gl_FrontColor.y = camera.z/500.0;
	
	//gl_FrontColor.x = length(pos-cam)/400.0 - 0.5;
	gl_FrontColor.x = 1.0 - (cos_a/2.0 + 0.5);
	gl_FrontColor.y = 1.0 - length(pos - center) / 100.0;
	gl_FrontColor.z = cos_a/2.0 + 0.5;
	//gl_FrontColor.z = 0.5 + cos_a*0.000001;
	micolor = gl_FrontColor;
	//gl_FrontColor.y = t;
	//gl_FrontColor.z = 0.5;
}

