#include "Gestor.h"

Gestor::Gestor(int width, int height):Ventana(width, height),ShaderManager()
{
	SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
	point_size = 1;
	initGL();
	if (!initShaders()) {
		exit(1);
	}
	loadShaders("./shaders/jaco.vs.glsl", "./shaders/depth.fs.glsl");

	t = 0;
	setFps(200);

	xPoints= 30;
	yPoints= 30;
	pointsWidth= 50;
	pointsHeight= 50;
	precision = 10;
	plot = nullptr;

	//plot->points.reserve(10000);
	//userPointsSetup = NULL;
	//resetVector();

	//plot->points = {1, 1, 1};
	drot = 1;
	dtrl = 1;
	semaforoDisplay.abrir();

	vldr.setOrientacion(Vector3D(-1, 0, 0).Unit(), Vector3D(0, 0, 1));
	vldr.setPos(Vector3D(500, 50, 100));

	arrastre = false;
	rollerCoaster = false;
	numThreads = 4;
	currentShader = 0;
	threshold = 220;
	autoRotate = false;
	rotationToCenter = false;
	//omp_set_num_threads(numThreads);
	//cout << "omp_get_num_threads() = " << omp_get_num_threads() << endl;	// DEPURACION
}


/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL()
{
	GLdouble aspect;
	int width = 640, height = 480;

	if (window == NULL)
		SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION
				, "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
	else
	{
		SDL_GetWindowSize(window, &width, &height);
		context = SDL_GL_CreateContext(window);
		if (!context) {
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
			SDL_Quit();
			exit(2);
		}
	}

	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
	glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
	glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // Reset The Projection Matrix

	aspect = (GLdouble)width / height;

	perspectiveGL (30, aspect, 1, 1000);
	//glOrtho(-200, 200, -100, 100, -1000, 100);
	glPointSize(point_size);

	//glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
}

void Gestor::onStep(float dt)
{
	t++;
	dt = dt/precision * 10;
	//for (int i = 0; i < 10; i++)
	//AtractorFunc(plot->points[i], dt);

	//Vector3D posAnt = plot->points[0];
	center *= 0;
	unsigned int historySteps = 1000;
	switch (plot->plotType) {
		case HISTORY:
			for (unsigned int i = 0; i < historySteps; i++) {
				plot->points[(t*historySteps + i)%plot->points.size()] = plot->points[(t*historySteps+i+plot->points.size()-1)%plot->points.size()];
				plot->iterationFunction(plot->points[(t*historySteps+i)%plot->points.size()], dt, 0);
				center = plot->points[(t*historySteps+i)%plot->points.size()];
			}
			break;
		case TRAJECTORY:
			//parallelComputation(4, dt);	// decepcionante
			for(unsigned int t = 0; t < plot->points.size(); t++) {
				plot->iterationFunction(plot->points[t], dt, t);
				center += plot->points[t];
			}
			break;

		default:
			break;
	}

	//center = plot->points[0];
	center /= plot->points.size();

	//#pragma omp parallel for num_threads(4)
	//cout << "isPressed('g') = " << isPressed('g') << endl;	// DEPURACION
	//cout << "isPressed('G') = " << isPressed('G') << endl;	// DEPURACION
	//cout << "isPressed(SDL_SCANCODE_G) = " << isPressed(SDL_SCANCODE_G) << endl;	// DEPURACION
	if (rollerCoaster) {
		vldr.setPos(plot->points[0] + vldr.getUp()*0.2);
	}

	//vldr.setOrientacion(plot->points[0]-posAnt, vldr.getUp());

}

/*
   template <typename VecIter>
   void Gestor::task(VecIter begin, VecIter end, float dt) {
   for (auto t = begin; t < end; t++) {
   (*atractorFunc)(*t, dt);
   }
   }
   */
/*
   void Gestor::task(decltype(plot->points.begin()) begin, decltype(plot->points.begin()) end, float dt) {
   for (auto t = begin; t < end; t++) {
   (*atractorFunc)(*t, dt);
   }
   }
   */
/*
   void Gestor::parallelComputation (int numThreads, float dt) {

//auto firstHandle = std::async(std::launch::async, task, plot->points.begin(), plot->points.end());

//template <typename Iterator> auto add(Iterator b, Iterator a) { 
//for (auto t = beg; t < end; t++) {
//(*atractorFunc)(*t, dt);
//}
//};
//

//std::future<void> *handle[3];
int t;
auto len = std::distance(plot->points.begin(), plot->points.end());

//handle[0] = &firstHandle;
for (t = 0; t < numThreads; t++) {
task(plot->points.begin() + t*(len/numThreads), plot->points.begin() + (t+1)*(len/numThreads), dt);
//task(plot->points[n/numThreads*t], &plot->points[n/numThreads*(t+1)], dt);
}
}
*/

void Gestor::drawAxes()
{
	glPushMatrix();
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(100, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 2, 0);
	glVertex3f(0, 2, 1);
	glEnd();
	glPopMatrix();
}


void Gestor::onDisplay()
{
	// Set the background black
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	// Clear The Screen And The Depth Buffer
	if (arrastre)
		glClear( GL_DEPTH_BUFFER_BIT );
	else
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );



	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	glLoadIdentity();                // Reset The View
	vldr.Look();

	//static float accumulatedAngle = 0;
	if (rotationToCenter) {
		if (autoRotate) {
			/* //with eigen 
			   Vector3f e(vldr.getPos().v);
			   Vector3f ce((vldr.getPos() - center).v);
			   Vector3f c(center.v);
			   Vector3f up(vldr.getUp().v);
			//Transform<float,3,Affine> t = Translation3f(c) * AngleAxisf(drot/100.0, up) * Translation3f(-c);
			Transform<float,3,Affine> t(AngleAxisf(drot/100.0, up));
			Vector3f newPos = t * ce;
			vldr.setPos(Vector3D(newPos.x(), newPos.y(), newPos.z()) + center);
			vldr.setOrientacion(center - vldr.getPos(), vldr.getUp());
			*/
			vldr.rotateAround(center, drot, 0, 1, 0);
		} else {
			vldr.rotateAround(center, 0, 0, 1, 0);
		}
	}

	drawAxes();

	//glTranslatef(center.x, center.y, center.z);
	if (shadersSupported) {
		selectShader(currentShader);
		//float camera[3] = {vldr.getPos().v[0], vldr.getPos().v[1], vldr.getPos().v[2]};
		//float val = fabs(vldr.getPos().y);
		//val = val > 100 ? 100: val;
		//val /=100;
		//cout << "val = " << val << endl;	// DEPURACION
		//GLfloat camera[3] = {0.5, val, 0.5};
		//const GLfloat camera[3] = {1.0, 0.5, 1.0};
		//const string cameraVarName = "camera";
		//GLint location = glGetUniformLocationARB(shaders[currentShader].program, "camera");
		//if (location >= 0) {
		//glUniform2fvARB(location, 2, camera);
		//} else {
		//cout << "variable camera not found" << endl;
		//}
		GLint location;
		float camx = vldr.getPos().x;
		float camy = vldr.getPos().y;
		float camz = vldr.getPos().z;
		location = glGetUniformLocationARB(shaders[currentShader].program, "camx");
		if (location >= 0) {
			glUniform1fARB(location, camx);
		} else {
			cout << "variable camx not found" << endl;
		}
		location = glGetUniformLocationARB(shaders[currentShader].program, "camy");
		if (location >= 0) {
			glUniform1fARB(location, camy);
		} else {
			cout << "variable camy not found" << endl;
		}
		location = glGetUniformLocationARB(shaders[currentShader].program, "camz");
		if (location >= 0) {
			glUniform1fARB(location, camz);
		} else {
			cout << "variable camz not found" << endl;
		}

		location = glGetUniformLocationARB(shaders[currentShader].program, "centerx");
		if (location >= 0) {
			glUniform1fARB(location, center.x);
		} else {
			cout << "variable centerx not found" << endl;
		}
		location = glGetUniformLocationARB(shaders[currentShader].program, "centery");
		if (location >= 0) {
			glUniform1fARB(location, center.y);
		} else {
			cout << "variable centery not found" << endl;
		}
		location = glGetUniformLocationARB(shaders[currentShader].program, "centerz");
		if (location >= 0) {
			glUniform1fARB(location, center.z);
		} else {
			cout << "variable centerz not found" << endl;
		}




		/*
		   location = glGetUniformLocationARB(shaders[currentShader].program, "threshold");
		   if (location >= 0) {
		   glUniform1fARB(location, threshold);
		   } else {
		   cout << "variable threshold not found" << endl;
		   }
		   */

	} else {
		cout << "shaders not supported" << endl;
	}
	//location = glGetUniformLocationARB(shaders[currentShader].program, "t");
	//GLfloat mit = ((float)(t%120))/60.0;
	//if (location >= 0) {
	//glUniform1fARB(location, mit);
	//} else {
	//cout << "variable t not found" << endl;
	//}
	//cout << "vldr.getPos().v[0] = " << vldr.getPos().v[0] << endl;	// DEPURACION
	//cout << "vldr.getPos().v[1] = " << vldr.getPos().v[1] << endl;	// DEPURACION
	//cout << "vldr.getPos().v[2] = " << vldr.getPos().v[2] << endl;	// DEPURACION


	glPushMatrix();

	//glTranslatef( 0, sin(t*0.05), -5.0);
	//glTranslatef( 0, 0, -5.0);
	//glRotatef(t, 0, 1, 0);
	glColor4f(0, 0.0, 1, arrastre? 0.004: 0.5);
	//glColor4f(1, 1, 1, 1);

	//glBegin( GL_TRIANGLES ); // Drawing Using Triangles
	//glVertex3f( 0.0f, 1.0f, 0.0f ); // Top
	//glVertex3f( -1.0f, -1.0f, 0.0f ); // Bottom Left
	//glVertex3f( 1.0f, -1.0f, 0.0f ); // Bottom Right
	//glEnd( ); // Finished Drawing The Triangle

	glEnableClientState (GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(Vector3D), plot->points.data());
	switch (plot->plotType) {
		case HISTORY:
			glDrawArrays(GL_LINE_STRIP, 0, plot->points.size());
			break;
		case TRAJECTORY:
		default:
			glDrawArrays(GL_POINTS, 0, plot->points.size());
			break;
	}
	glDisableClientState(GL_VERTEX_ARRAY);

	//glBegin(GL_POINTS);
	//for(Vector3D v : plot->points) {
	//glVertex3fv(v.v);
	////glVertex3f(v.x, v.y, 0);
	//}
	//glEnd();

	glPopMatrix();
	if (shadersSupported) {
		unselectShader();
	}

	SDL_GL_SwapWindow(window);
}

void Gestor::onPressed(const Pulsado &p)
{
	if (p.sym == SDLK_u
			|| p.sym == SDLK_o
			|| p.sym == SDLK_i
			|| p.sym == SDLK_k
			|| p.sym == SDLK_j
			|| p.sym == SDLK_l
			|| p.sym == SDLK_w
			|| p.sym == SDLK_s
			|| p.sym == SDLK_a
			|| p.sym == SDLK_d
			|| p.sym == SDLK_q
			|| p.sym == SDLK_e)
	{
		//semaforoStep.cerrar();
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	}

	switch(p.sym)
	{
		case SDLK_u:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			if (rotationToCenter) {
				vldr.rotateAround(center, -drot, 0, 0, 1);
			} else {
				vldr.rotatef(-drot, 0, 0, 1);
			}
			break;
		case SDLK_o:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			if (rotationToCenter) {
				vldr.rotateAround(center, drot, 0, 0, 1);
			} else {
				vldr.rotatef(drot, 0, 0, 1);
			}
			break;
		case SDLK_i:
			//vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
			if (rotationToCenter) {
				vldr.rotateAround(center, drot, 1, 0, 0);
			} else {
				vldr.rotatef(drot, 1, 0, 0);
			}
			break;
		case SDLK_k:
			//vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
			if (rotationToCenter) {
				vldr.rotateAround(center, -drot, 1, 0, 0);
			} else {
				vldr.rotatef(-drot, 1, 0, 0);
			}
			break;
		case SDLK_l:
			if (rotationToCenter) {
				vldr.rotateAround(center, drot, 0, 1, 0);
			} else {
				vldr.rotY(-drot);
			}
			break;
		case SDLK_j:
			if (rotationToCenter) {
				vldr.rotateAround(center, -drot, 0, 1, 0);
			} else {
				vldr.rotY(drot);
			}
			break;
		case SDLK_e:
			vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
			break;
		case SDLK_q:
			vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
			break;
		case SDLK_w:
			vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
			break;
		case SDLK_s:
			vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
			break;
		case SDLK_d:
			vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
			break;
		case SDLK_a:
			vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
			break;
	}
}

void Gestor::onKeyPressed(SDL_Event &e)
{
	int entero;
	int ret;

	if (e.type == SDL_KEYDOWN)
		return;

	switch(e.key.keysym.sym)
	{
		case SDLK_SPACE:
			//semaforoDisplay.sumar();

			//semaforoStep.cerrar();
			//semaforoStep.sumar();
			if (semaforoStep.estado())
				semaforoStep.cerrar();
			else
				semaforoStep.abrir();
			break;
		case SDLK_p:	// play / stop
			if (semaforoDisplay.estado())
			{
				semaforoStep.cerrar();
				semaforoDisplay.cerrar();
				semaforoDisplay.sumar();
			}
			else
			{
				semaforoStep.abrir();
				semaforoDisplay.abrir();
			}
			break;
		case SDLK_h:
			//SDL_LOG();
			ret = system("cat src/help.txt");
			if (ret) {
				SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "help not found");
			}
			//cout << "Help:\n\t"
			//"r: redraw\n\t"
			//"c: frames per second (FPS)\n\t"
			//;	// TODO print_file("ayuda.txt");
			break;
		case SDLK_r:	// redraw 
			//semaforoDisplay.sumar();
			threshold *= (1+0.0625);
			break;
		case SDLK_f:
			threshold /= (1+0.0625);
			break;
		case SDLK_g:
			autoRotate = !autoRotate;
			break;
		case SDLK_t:
			rotationToCenter = !rotationToCenter;
			break;
		case SDLK_n:	// siguiente iteracion 
			semaforoStep.sumar();
			break;
		case SDLK_c:
			cout << "new FPS: " << endl;
			SDLcin(entero);
			setFps(entero);
			break;
		case SDLK_m:
			arrastre = !arrastre;
			break;
		case SDLK_v:
			plot->reset();
		case SDLK_b:
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
			break;
		case SDLK_x:
			cout << "Nueva precision (actual = " << precision << "):\n";
			SDLcin(precision);
			if (precision < 1) {
				precision = 1;
			}
			break;
		case SDLK_z:
			rollerCoaster = !rollerCoaster;
			break;
		case SDLK_PLUS:
			dtrl *= 2;
			break;
		case SDLK_MINUS:
			dtrl *= 0.5;
			break;
		default:
			break;
	}
}

void Gestor::onMouseButton(SDL_Event &e)
{
	last_click_x = e.button.x;
	last_click_y = e.button.y;

	//semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
	xant = e.button.x;
	yant = e.button.y;

	//semaforoDisplay.sumar();
}


void Gestor::setPlot(Plot &p) {
	plot = &p;
	plot->reset();
	//if (plot->pointConfig.pointsSetup != NULL) {
	//userPointsSetup = plot.pointConfig.pointsSetup;
	//} else {
	//defineVector (plot.pointConfig.nx, plot.pointConfig.ny, plot.pointConfig.width, plot.pointConfig.height);
	//}
}
