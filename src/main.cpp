
#include <vector>
#include "Gestor.h"




namespace PlotFunctions {
	void Lorenz(Vector3D &v, float dt)
	{
		float a, b, c, d;
		a = b = c = d = 1;
		a = 10;
		b = 100;
		c = 8.0/3.0;
		d = dt*0.02;
		float x = v.x + a*d*(v.y - v.x);
		float y = v.y + d*(b*v.x - v.y - v.x*v.z);
		float z = v.z + d*(v.x*v.y - c*v.z);
		//cout << "v = " << v << endl;	// DEPURACION
		v.setX(x);
		v.setY(y);
		v.setZ(z);
		//v = Vector3D(x, y, z);
		//cout << "v = " << v << endl;	// DEPURACION
	}

	void LorenzCroissant(Vector3D &v, float dt)
	{
		float a, b, c, d;
		a = b = c = d = 1;
		a = 10;
		b = 10;
		c = 8.0/3.0;
		d = dt*0.02;
		float x = v.x + a*d*(v.y - v.x);
		float y = v.y + d*(b*v.x - v.y - v.x*v.z);
		float z = v.z + d*(v.x*v.y - c*v.z);
		//cout << "v = " << v << endl;	// DEPURACION
		v = Vector3D(x, y, z);
		//cout << "v = " << v << endl;	// DEPURACION
	}
	/**
	 * Good values for c:
	 * 4, _6_, 8.5, 8.7, _9_, _12_, 12.6, 13, _18_
	 */
	void Rossler(Vector3D &v, float dt)
	{
		float a, b, c, d;
		a = 0.1;
		b = 0.1;
		c = 12;
		d = dt*0.4;
		float x = v.x + d*(-v.y - v.z);
		float y = v.y + d*(v.x + a*v.y);
		float z = v.z + d*(b + v.z*(v.x - c));
		//v.x = v.x + d*(-v.y - v.z);
		//v.y = v.y + d*(v.x + a*v.y);
		//v.z = v.z + d*(b + v.z*(v.x - c));
		//cout << "v = " << v << endl;	// DEPURACION
		v = Vector3D(x, y, z);
		//cout << "v = " << v << endl;	// DEPURACION
	}

	void Pickover(Vector3D &v, float dt)
	{
		float a, b, c, d;
		a = 0.2;
		b = 0.2;
		c = 5.7;
		d = dt*0.0004;
		float x = sin(a*v.y) - v.z*cos(b*v.x);
		float y = v.z*sin(c*v.x) - cos(d*v.y);
		float z = sin(v.x);
		//cout << "v = " << v << endl;	// DEPURACION
		v = Vector3D(x, y, z);
		//cout << "v = " << v << endl;	// DEPURACION
	}

	void Senos (Vector3D &v, float dt)
	{
		//float x = v.x - sin(tiempo - dt)*1;
		//x += sin(tiempo);
		float x = v.x;
		float y = v.y;
		float z = v.z + dt*1;
		v = Vector3D(x, y, z);
	}

	const float ln_phi = log(1.61803398875); 
	//const float ln_phi = (1.61803398875); 
	void Phi (Vector3D &v, float dt)
	{
		dt *= 10;
		float x = v.x*(dt+1);
		float y = v.y;
		float z = v.z + dt*1;
		v = Vector3D(x, y, z);
	}
	void PhiEspiral (Vector3D &v, float dt)
	{
		dt *= 5;
		float amplitud = sqrt(v.x*v.x + v.y*v.y);
		float x = cos(v.z)*amplitud*(ln_phi*dt/(2*M_PI)+1);
		float y = sin(v.z)*amplitud*(ln_phi*dt/(2*M_PI)+1);
		float z = v.z + dt*1;
		v = Vector3D(x, y, z);
	}

	void Espiral (Vector3D &v, float dt)
	{
		dt *= 10;
		float amplitud = sqrt(v.x*v.x + v.y*v.y);
		float x = v.x + cos(amplitud)*dt;
		float y = v.y + sin(amplitud)*dt;
		float z = v.z + dt*1;
		v = Vector3D(x, y, z);
	}

	const float G = 1000;	// Gravitational constant

	class PlotGravedad : public Plot {
		private:
			vector<Vector3D> speeds;

		public:
			PlotGravedad(PlotType pt, PointConfig pc) : Plot(pt, pc), speeds(pc.nx*pc.ny){
				userReset();
			}
			void userReset(){
				for (Vector3D & v : speeds) {
					v = Vector3D(0, -5, 0);
				}
			}

			void iterationFunction (Vector3D &v, float dt, int index) {
				static vector<Vector3D> points = {{0, 20, 0}, {50, 0, 0}, {0, 0, 80}};
				Vector3D forces = {0, 0, 0};
				Vector3D r;
				float dist;
				float m = 1;
				Vector3D vel_0 = speeds[index];
				Vector3D x_0 = v;

				for (Vector3D p : points) {
					r = p - v;
					dist = r.Modulo();
					forces += G*m*m/(dist*dist)*r.Unit();
				}

				//cout << "forces = " << forces << endl;	// DEPURACION
				Vector3D a = forces/m;
				Vector3D vel = a*dt + vel_0;
				Vector3D x = a*dt*dt/2 + vel_0*dt + x_0;
				//cout << "x-v = " << x-v << endl;	// DEPURACION
				v = x;
				speeds[index] = vel;
			}
	};
};

using namespace PlotFunctions;

int main(int argc, char **argv)
{
	Gestor gestor(1300, 700);
	int x = 500, y = 500;
	int n = 100000;
	unsigned int numFunc;
	bool defaultFunction = true;
	PointConfig matrix = {x, y, 50, 50, NULL};
	PointConfig line = {n, 1, 25, 1, NULL};
	PointConfig random = {n, 1, 1, 1, [](vector<Vector3D> &v){
		int n = 100000;
		int bound = 60;
		float div = 10000.0;
		v.resize(n);
		cout << "en random" << endl;
		for (Vector3D &p : v) {
			p.x = remainder(rand()/div, bound);
			//p.x= 0;
			p.y = remainder(rand()/div, bound);
			p.z = remainder(rand()/div, bound);
		}
	}};
	
	vector<Plot*> plots = {
		new Plot{TRAJECTORY, matrix, Lorenz},
		new Plot{HISTORY, random, Lorenz},
		new Plot{TRAJECTORY, random, Lorenz,},
		new Plot{TRAJECTORY, line, Rossler},
		new Plot{TRAJECTORY, random, Rossler},
		new Plot{TRAJECTORY, matrix, Pickover},
		new Plot{TRAJECTORY, matrix, Senos},
		new Plot{TRAJECTORY, matrix, Phi},
		new Plot{TRAJECTORY, line, PhiEspiral},
		new Plot{TRAJECTORY, matrix, Espiral},
		new PlotGravedad(TRAJECTORY, random),
		new PlotGravedad(HISTORY, line),
	};

	if (argc == 2) {
		numFunc = atoi(argv[1]);
		if (numFunc < plots.size()) {
			defaultFunction = false;
			gestor.setPlot (*plots[numFunc]);
		} else {
			cout << "that function doesn't exist: try from 0 to " << plots.size()-1 << endl;
		}
	}

	if (defaultFunction) {
		gestor.setPlot (*plots[0]);
	}

	gestor.mainLoop();

	return 0;
}
/*

   vector<void (*)(Vector3D &v, float dt)> funcs = {
   Lorenz,
   Rossler,
   Pickover,
   Senos,
   Phi,
   PhiEspiral,
   Espiral
   };
   enum TipoPlot {
   MATRIZ = 30,
   LINEA = 1
   };
   vector<TipoPlot> tipo = {
   MATRIZ,
   LINEA,
   MATRIZ,
   MATRIZ,
   MATRIZ,
   LINEA,
   MATRIZ
   };
   bool defecto = true;
   unsigned int numFunc;

   if (argc == 2)
   {
   numFunc = atoi(argv[1]);
   if (numFunc < funcs.size())
   {
   gestor.resetVector();
   gestor.atractorFunc = funcs[numFunc];
   defecto = false;
   }
   else
   {
   cout << "that function doesn't exist: try from 0 to " << funcs.size()-1 << endl;
   }
   }

   if (defecto) {
   cout << "using default configuration" << endl;
   gestor.defineVector(100000, 1, 20);
   gestor.atractorFunc = Rossler;
   }

*/


