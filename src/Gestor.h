#ifndef _GESTOR_H_
#define _GESTOR_H_

#include <vgm/Ventana.h>
#include <vgm/ShaderManager.h>
#include <vector>
#include <algorithm>
#include <numeric>
#include <future>
#include <iterator>

#include "Memoria.h"


class Gestor: public Ventana, public Memoria, public ShaderManager
{
	public:
		Gestor(int width = 640, int height = 480);
		void initGL();
		void onStep(float);
		void onDisplay();
		void onKeyPressed(SDL_Event &e);
		void onPressed(const Pulsado &p);
		void onMouseMotion(SDL_Event &e);
		void onMouseButton(SDL_Event &e);

		void setPlot(Plot &plot);
		//void defineVector(float x = 30, float y = 30, float width = 10, float height = 10);
		//void resetVector();

	private:
		void drawAxes();
		//void resetVector(vector<Vector3D> &v, float x = 30, float y = 30, float width = 10, float height = 10);
		//template <typename VecIter> void task(VecIter begin, VecIter end, float dt);
		//void task(decltype(punto.begin()) begin, decltype(punto.begin()) end, float dt);
		//void parallelComputation (int numThreads, float dt);
};
#endif
