#ifndef _MEMORIA_H_
#define _MEMORIA_H_

#include <SDL.h>
#include <complex>
#include <graphics/drawables/Volador.h>
#include <math/Vector3D.h>
//#include <Geometry>
#include "Plot.h"
//using namespace Eigen;

struct Memoria
{
	int last_click_x, last_click_y;
	int xant, yant;
	int t;
	Volador vldr;
	float drot, dtrl;
	float xPoints, yPoints, pointsWidth, pointsHeight;
	//void Plot::(*atractorFunc)(Vector3D &v, float dt);
	//PlotType plotType;
	//void (*userPointsSetup)(vector<Vector3D> &v);
	bool arrastre;
	float point_size;
	int precision;
	bool rollerCoaster;
	int numThreads = 4;
	float threshold;
	Plot *plot;
	Vector3D center;
	bool autoRotate;
	bool rotationToCenter;
	//Eigen::Quaternionf q;
};


#endif
