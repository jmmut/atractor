#ifndef _PLOT_H_
#define _PLOT_H_

#include <vector>
#include <math/Vector3D.h>
using namespace std;
enum PlotType {
	TRAJECTORY,
	HISTORY
};

struct PointConfig {
	int nx, ny;
	int width, height;
	void (*pointsSetup) (vector<Vector3D> &points);
};

class Plot {
	public:
	//private:
		vector<Vector3D> points;
		PlotType plotType;
		PointConfig pointConfig;
		void (*myIterationFunction)(Vector3D&, float);
	public:
		Plot(PlotType pt, PointConfig pc);
		Plot(PlotType pt, PointConfig pc, void (*iterationFunction)(Vector3D&, float));
		virtual void iterationFunction (Vector3D &v, float dt, int index);
		virtual void userReset();
		void reset();
};

#endif // _PLOT_H_

