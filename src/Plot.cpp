
#include "Plot.h"

Plot::Plot(PlotType pt, PointConfig pc) : plotType(pt), pointConfig(pc) {
}

Plot::Plot(PlotType pt, PointConfig pc, void (*iterationFunction)(Vector3D&, float))
	: plotType(pt), pointConfig(pc), myIterationFunction(iterationFunction) {
}

void Plot::iterationFunction (Vector3D &v, float dt, int index) {
	myIterationFunction(v,dt);
}

void Plot::userReset(){
}

void Plot::reset() {
	userReset();
	cout << "pointConfig.pointsSetup = " << pointConfig.pointsSetup << endl;	// DEPURACION
	if (pointConfig.pointsSetup) {
		pointConfig.pointsSetup(points);
	} else {
		int elem = 0;
		float startX = 0.1;
		float startY = 0;
		float rangeX = startX + pointConfig.width;
		float rangeY = startY + pointConfig.width;
		float incX = (rangeX - startX)/pointConfig.nx;
		float incY = (rangeY - startY)/pointConfig.ny;

		points.resize(pointConfig.nx*pointConfig.ny);
		for (int ix = 0; ix < pointConfig.nx; ix++) {
			for (int iy = 0; iy < pointConfig.ny; iy++) {
				points[ix*pointConfig.ny + iy] = Vector3D(ix*incX + startX, iy*incY + startY, 0);
				elem++;
			}
		}
		cout << elem << " puntos seteados" << endl;
	}
}


