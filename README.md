
# Atractor

This program plots any R^3 function along time.

It was conceived to plot atractors, but can be used with other functions as well, like gravitation ones.

This is a video of an execution with the Lorenz attractor:
[lorenz](https://drive.google.com/file/d/0B6TTJXRF1hACSmUxNzdmS3I2Tk0/view?usp=sharing)


# Installing dependencies

This program requires SDL2 and randomize/common-libs. In debian-like distros, SDL2 is available in apt-get:
```
sudo apt-get install libsdl2-dev
```

randomize/common-libs can be downloaded with wget or git:
```
wget https://bitbucket.org/mutcoll/randomize/get/master.zip; unzip master.zip
```
or 
```
git clone https://bitbucket.org/mutcoll/randomize.git
```
or 
```
git clone git@bitbucket.org:mutcoll/randomize.git
```
and then:
```
cd randomize/common-libs
make
sudo make install
cd ../../
```

# Installing

you can build the program with make, the file is in the src directory

```
cd src
make
cd ..
```

finally, to launch:
```
./atractor
```

You can also pass a number, and will plot several pre-defined functions:
```
./atractor 10
```

