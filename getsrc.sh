#!/bin/bash
ORIG_DIRS=(~/libs/visor2/ ~/libs/vector/sinmodulo/ ~/libs/volador/)
NEW_DIRS=(src/visor/ src/vector/ src/volador/)
i=0
while [ $i -lt ${#ORIG_DIRS[@]} ]
do
    mkdir -p ${NEW_DIRS[$i]}
    for file in $(ls ${ORIG_DIRS[$i]})
    do
        ln -s ${ORIG_DIRS[$i]}$file ${NEW_DIRS[$i]}$file
    done
    let i=i+1
done

